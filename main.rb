# Function to determine if at specified posizion and size square is magic
def is_magic?(x, y, size, board)
  row_sum = Array.new(size, 0)
  col_sum = Array.new(size, 0)
  cross_ltr = 0
  cross_rtl = 0
  # Calculate square sums
  for h in 0..(size - 1)
    for w in 0..(size - 1)
      row_sum[h] += board[y + h][x + w]
      col_sum[w] += board[y + h][x + w]
      if h == w
        cross_ltr += board[y + h][x + w]
        cross_rtl += board[y + h][x + (size - 1 - w)]
      end
    end
  end
  # Merge arrays
  result = [] << cross_ltr << cross_rtl
  result += row_sum + col_sum
  # Check if there is only one unique value
  return false if result.uniq.length != 1
  return true
end

# Find the number of magic squares in given board
def solve(board)
  result = 0
  # Dimensions
  width = board[0].length
  height = board.length
  # Traverse through all possible squares with minimum 2 size
  for y in 0..(height - 2)
    for x in 0..(width - 2)
      size = [height- y, width - x].min
      for s in 2..size
        result += 1 if is_magic?(x, y, s, board)
      end
    end
  end
  return result
end

# Main method
def main
  board = []
  # Read Data
  File.open('input.txt') do |input|
    input.each_line do |line|
      board << line.split.map(&:to_i)
    end
  end
  print "Number of magic squares: "
  puts solve(board)
end

# Run main method
main
